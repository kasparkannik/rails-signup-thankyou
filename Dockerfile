FROM ruby:2.5.1
MAINTAINER kaspar.kannik@outlook.com

ENV BUNDLER_VERSION=1.16.2

RUN apt-get update && apt-get install -y \
      apt-utils \
      build-essential \
      binutils-gold \
      curl \
      file \
      libstdc++ \
      libffi-dev \
      libc-dev \ 
      libxml2-dev \
      libxslt-dev \
      libgcrypt-dev \
      make \
      nodejs \
      postgresql-client \
      tzdata \
      yarn \
      vim cron gdebi > /dev/null

RUN gem install bundler -v 1.16.2
WORKDIR /app
COPY Gemfile Gemfile.lock ./
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle check || bundle install
COPY package.json ./
COPY . ./ 
ENTRYPOINT ["./entrypoints/docker-entrypoint.sh"]

